create table accounts (
    username varchar(255) not null primary key,
    password varchar(255),
    fullname varchar(255) not null,
    email varchar(255) not null,
    dob date not null
);

drop table records;

create table records (
    event varchar(255) not null primary key,
    organizer varchar(255) not null,
    location varchar(255) not null,
    date varchar(255) not null
);


insert into accounts (username, password, fullname, email, dob) values
    ('test', 'test', 'Test pradhan', 'test@example.com', {d '1996-01-01'});

insert into records (event, organizer, location, date) values
    ('testevent','testorganizer','testlocation',{d '1996-01-01'});


select * from records;

create view jdbcrealm_user (username, password) as
select username, password
from accounts;

create view jdbcrealm_group (username, groupname) as
select username, 'Users'
from accounts;

select * from jdbcrealm_user