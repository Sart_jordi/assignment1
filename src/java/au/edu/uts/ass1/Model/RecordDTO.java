package au.edu.uts.ass1.Model;

import java.io.*;

/**
 * A Data Transfer Object used to communicate with the Account Data Access Object.
 */
public class RecordDTO implements Serializable{

    private String event;
    private String organizer;
    private String location;
    private String date;

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
     
    }
    
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
}
