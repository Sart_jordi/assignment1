
package au.edu.uts.ass1.DAO;

import java.sql.*;
import java.util.*;
import javax.naming.*;
import javax.sql.*;
import au.edu.uts.ass1.Model.RecordDTO;
import au.edu.uts.ass1.Helper.DataStoreException;


/**
 * A Data Access Object to access data in the Account table.
 */
public class RecordDAO {
    
    
    private static final String JNDI_NAME = "jdbc/ass";
    
    
    
    private static final String CREATE_RECORD =
            "insert into records (event, organizer, location, date) values (?,?,?,?)";
    
    private static final String DELETE_RECORD =
            "delete from records where event = ?";
    
    private static final String SELECT_RECORD =
            "select event,organizer,location,date " +
            "from records ";
    private static final String RECORD_ALL = SELECT_RECORD;
    
   
    private static final String RECORD_USERNAME = SELECT_RECORD + " where username = ?";
    
    
    
    private static final String UPDATE_RECORD = "UPDATE records SET organizer = ?" +
            ", location = ?" +
             ",date = ?" +  
            "WHERE event = ?";
            
    /**
     * Helper-method to create a Data Transfer Object for a row of the database.
     * @param rs the ResultSet from which to retrieve the attributes for the DTO
     * @return a DTO corresponding to the current row of the ResultSet
     * @throws SQLException
     */
    private RecordDTO createRowDTO(ResultSet rs) throws SQLException {
        RecordDTO result = new RecordDTO();
        result.setEvent(rs.getString("event"));
        result.setOrganizer(rs.getString("organizer"));
        result.setLocation(rs.getString("location"));
        result.setDate(rs.getString("date"));
        return result;
    }
    
     /**
     * Retrieves all accounts from the database as a complete list of Data Transfer Objects.
     * @return a list containing every row of the database
     * @throws DataStoreException if an exception occurred while communicating with the database.
     */
    public ArrayList<RecordDTO> findAll() throws DataStoreException {
        ArrayList<RecordDTO> results = new ArrayList<>();
        try {
            DataSource ds = InitialContext.doLookup(JNDI_NAME);
            try (Connection conn = ds.getConnection();
                 Statement stmt = conn.createStatement();
                 ResultSet rs = stmt.executeQuery(RECORD_ALL)) {

                while (rs.next()) {
                    results.add(createRowDTO(rs));
                }
            }
        } catch (NamingException | SQLException e) {
            throw new DataStoreException(e);
        }
        return results;
    }
    
    /**
     * Retrieves the account in the database, corresponding to a given username.
     * @param username This is the only parameter to supply the username
     * @return a Data Transfer Object corresponding to the username, or null if no matching user was found
     * @throws DataStoreException if an exception occurred while communicating with the database.
     */
    public RecordDTO findEvent(String username) throws DataStoreException {
        try {
            DataSource ds = InitialContext.doLookup(JNDI_NAME);
            try (Connection conn = ds.getConnection();
                 PreparedStatement ps = conn.prepareStatement(RECORD_USERNAME)) {

                ps.setString(1, username);
                
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        // username found
                        return createRowDTO(rs);
                    } else {
                        // user not found
                        return null;
                    }
                }
            }
        } catch (NamingException | SQLException e) {
            throw new DataStoreException(e);
        }
    }
    
     /**
     * Method to insert a set of record to the database based on the parameters supplied.
     * @param event This is the first parameter.
     * @param organizer This is the second parameter.
     * @param location This is the fourth parameter.
     * @param date This is the fifth parameter.
     * @return a list containing every row of the database
     * @throws DataStoreException if an exception occurred while communicating with the database.
     */
    public void insert(String event, String organizer, String location, String date) throws DataStoreException{
        try {
            DataSource ds = InitialContext.doLookup(JNDI_NAME);
            try (Connection conn = ds.getConnection();
                 PreparedStatement ps = conn.prepareStatement(CREATE_RECORD)) {

               ps.setString(1, event);
               ps.setString(2, organizer);
               ps.setString(3, location);
               ps.setString(4, date);
               ps.executeUpdate();
                
            }
        } catch (NamingException | SQLException e) {
            throw new au.edu.uts.ass1.Helper.DataStoreException(e);
        }
        
        
        
    }

    /**
     * Method to remove a set of record on the database based on the parameters supplied.
     * @param event This is the first parameter.
     * @throws DataStoreException if an exception occurred while communicating with the database.
     */
    public void remove(String event) throws DataStoreException{
        try {
            DataSource ds = InitialContext.doLookup(JNDI_NAME);
            try (Connection conn = ds.getConnection();
                 PreparedStatement ps = conn.prepareStatement(DELETE_RECORD)) {

              ps.setString(1, event);
               ps.executeUpdate();
                
            }
        } catch (NamingException | SQLException e) {
            throw new au.edu.uts.ass1.Helper.DataStoreException(e);
        }
        
        
        
    }
    
    /**
     * Method to update a set of record to the database based on the event name.
     * @param event This is the first parameter.
     * @param organizer This is the second parameter.
     * @param location This is the fourth parameter.
     * @param date This is the fifth parameter.
     * @throws DataStoreException if an exception occurred while communicating with the database.
     */
    public void update(String event, String organizer, String location, String date) throws DataStoreException{
        try {
            DataSource ds = InitialContext.doLookup(JNDI_NAME);
            try (Connection conn = ds.getConnection();
                 PreparedStatement ps = conn.prepareStatement(UPDATE_RECORD)) {

               ps.setString(1, organizer);
               ps.setString(2, location);
               ps.setString(3, date);
               ps.setString(4, event);
               ps.executeUpdate();               
            }
        } catch (NamingException | SQLException e) {
            throw new au.edu.uts.ass1.Helper.DataStoreException(e);
        }
        
        
        
    }
    
}
