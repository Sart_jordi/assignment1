package au.edu.uts.ass1.DAO;

import java.sql.*;
import java.util.*;
import javax.naming.*;
import javax.sql.*;
import au.edu.uts.ass1.Model.AccountDTO;
import au.edu.uts.ass1.Helper.DataStoreException;

/**
 * A Data Access Object to access data in the Account table.
 */
public class AccountDAO {
    
    // Configuration ------------------------------------
    
    private static final String JNDI_NAME = "jdbc/ass";
    
    private static final String SELECT_ACCOUNTS =
            "select username, password, fullname, email, dob " +
            "from accounts ";
    private static final String ACCOUNT_ALL = SELECT_ACCOUNTS;
    private static final String ACCOUNT_USERNAME = SELECT_ACCOUNTS + " where username = ?";
    
    // Implementation ------------------------------------
    
    /**
     * Helper-method to create a Data Transfer Object for a row of the database.
     * @param rs the ResultSet from which to retrieve the attributes for the DTO
     * @return a DTO corresponding to the current row of the ResultSet
     * @throws SQLException
     */
    private AccountDTO createRowDTO(ResultSet rs) throws SQLException {
        AccountDTO result = new AccountDTO();
        result.setUsername(rs.getString("username"));
        result.setPassword(rs.getString("password"));
        result.setFullname(rs.getString("fullname"));
        result.setEmail(rs.getString("email"));
        result.setDob(rs.getDate("dob"));
        return result;
    }

    /**
     * Retrieves all accounts from the database as a complete list of Data Transfer Objects.
     * @return a list containing every row of the database
     * @throws DataStoreException if an exception occurred while communicating with the database.
     */
    public ArrayList<AccountDTO> findAll() throws DataStoreException {
        ArrayList<AccountDTO> results = new ArrayList<>();
        try {
            DataSource ds = InitialContext.doLookup(JNDI_NAME);
            try (Connection conn = ds.getConnection();
                 Statement stmt = conn.createStatement();
                 ResultSet rs = stmt.executeQuery(ACCOUNT_ALL)) {

                while (rs.next()) {
                    results.add(createRowDTO(rs));
                }
            }
        } catch (NamingException | SQLException e) {
            throw new DataStoreException(e);
        }
        return results;
    }
    
     /**
     * Retrieves the account in the database, corresponding to a given username.
     * @return a Data Transfer Object corresponding to the username, or null if no matching user was found
     * @throws DataStoreException if an exception occurred while communicating with the database.
     */
    public AccountDTO findUser(String username) throws DataStoreException {
        try {
            DataSource ds = InitialContext.doLookup(JNDI_NAME);
            try (Connection conn = ds.getConnection();
                 PreparedStatement ps = conn.prepareStatement(ACCOUNT_USERNAME)) {

                ps.setString(1, username);
                
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        // username found
                        return createRowDTO(rs);
                    } else {
                        // user not found
                        return null;
                    }
                }
            }
        } catch (NamingException | SQLException e) {
            throw new DataStoreException(e);
        }
    }
    
}
