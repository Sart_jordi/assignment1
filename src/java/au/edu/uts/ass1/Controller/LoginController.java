
package au.edu.uts.ass1.Controller;

import java.sql.*;
import java.util.*;
import java.util.logging.*;
import javax.enterprise.context.*;
import javax.faces.application.*;
import javax.faces.context.*;
import javax.inject.*;
import javax.naming.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.sql.*;
import au.edu.uts.ass1.Model.AccountDTO;
import au.edu.uts.ass1.DAO.AccountDAO;

import au.edu.uts.ass1.Helper.DataStoreException;




/**
 * A backing bean used for account management in our application.
 * @author Sarthak
 * @version 1.0
 * @since 2016-08-12
 */

@Named
@RequestScoped
public class LoginController {
    
    //Fields for username and password attributes of login.
    private String username;
    private String password;
    
    //getters and setters of the fields.
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * This method is used to control login of user by checking 
     * the user supplied username password with database.
     * @return
     * @throws DataStoreException 
     */
    public String login() throws DataStoreException {
        AccountDAO dao = new AccountDAO();
        AccountDTO user = dao.findUser(username);
        if (password.equals(user.getPassword())) {
            return "/ListEvent?faces-redirect=true";
        } else {
            showError("Incorrect username or password");
            return null;
        }
        
        
        
    }
    
    /**
     * This method is used to control the logout of the user.
     * @return
     * @throws ServletException 
     */
    public String logout() throws ServletException {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        request.logout();
        return "/index?faces-redirect=true";
    }
    
    /**
     * Adds a message to the current faces context, so that it will appear in
     * a h:messages element.
     * @param message the text of the error message to show the user
     */
    private void showError(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }
    
}
