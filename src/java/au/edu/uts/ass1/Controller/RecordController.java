
package au.edu.uts.ass1.Controller;

import java.sql.*;
import java.util.*;
import java.util.logging.*;
import javax.enterprise.context.*;
import javax.faces.application.*;
import javax.faces.context.*;
import javax.inject.*;
import javax.naming.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.sql.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import au.edu.uts.ass1.Model.RecordDTO;
import au.edu.uts.ass1.DAO.RecordDAO;

import au.edu.uts.ass1.Helper.DataStoreException;




/**
 * A backing bean used for record management in our application.
 * @author Sarthak
 * @version 1.0
 * @since 2016-08-12
 */
@Named
@RequestScoped
public class RecordController {
    
    //various fieds for type record.
    private String event;
    private String organizer;
    private String location;
    private String date;

    
    //getters and setters
    @Size(min=1)
    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
    @Size(min=1)
    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
     
    }
    
    @Size(min=1)
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    
    @Size(min=1)
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    
    public ArrayList<RecordDTO> getAllEvents() throws au.edu.uts.ass1.Helper.DataStoreException {
        return new RecordDAO().findAll();
    }
    
    
//    public RecordDTO getEvents() throws au.edu.uts.ass1.records.DataStoreException {
//        return new RecordDAO().findEvent(event);
//    }
    
    /**
     *This method is used to invoke the insert method in DAO, which
     * creates an record in the database
     * @return String This returns to ListEvent jsf if the data is successfully created.
     * @throws au.edu.uts.ass1.records.DataStoreException
     */
    public String create() throws au.edu.uts.ass1.Helper.DataStoreException {
     
        RecordDAO dao = new RecordDAO();
        dao.insert(event,organizer,location,date);
        
        return "/ListEvent?faces-redirect=true";
    }
    
    /**
     *This method is used to invoke the update method in DAO, which
     * updates a record in the database
     * @return String This returns to ListEvent jsf if the data is successfully created.
     * @throws au.edu.uts.ass1.records.DataStoreException
     */
    public String edit() throws au.edu.uts.ass1.Helper.DataStoreException {
     
        RecordDAO dao = new RecordDAO();
        dao.update(event,organizer,location,date);
        
        return "/ListEvent?faces-redirect=true";
    }
    
    /**
     *This method is used to invoke the delete method in DAO, which
     * deletes a record in the database
     * @return String This returns to ListEvent jsf if the data is succesfully created.
     * @throws au.edu.uts.ass1.records.DataStoreException
     */
    public String delete() throws au.edu.uts.ass1.Helper.DataStoreException {
        
        RecordDAO dao = new RecordDAO();
        dao.remove(event);
       
        return "/ListEvent?faces-redirect=true";
  }
    

//method to create a record
    
}
